/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class InsertUser {

public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (4, 'MAMAY', 'MAMAY')";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (5, 'KANTI', 'KANTI')";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (6, 'PLATHONG', 'PLATHONG')";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO COMPANY (ID,USERNAME,PASSWORD) "
                    + "VALUES (7, 'Sunflower, 'SUN')";
            stmt.executeUpdate(sql);

            stmt.close();
            conn.commit();
            conn.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

